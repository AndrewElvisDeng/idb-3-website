import React, { forwardRef, useEffect, useState } from 'react';
import axios from 'axios';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import tempImg from '../images/boltEV.jpg';
import { Link } from "react-router-dom";
// import { TwitterTimelineEmbed } from "react-twitter-embed";
import twitterLogo from '../images/twitterlogo.png';
import MaterialTable from "material-table";
import { Search, FirstPage, LastPage, ChevronLeft, ChevronRight, Clear, ArrowDownward } from "@material-ui/icons";

function VehicleInstance() {
  let pages = window.location.pathname.split('/');
  let id = pages[2];
  console.log(id);

  const [data, setData] = useState([]);
  const [stationRows, setStationRows] = useState([]);
  
  useEffect(() => {
    axios.get(`/api/vehicles/id=${id}`)
    .then((r) => {
      setData(r.data);


      if (r.data['fuel_stations'] !== undefined) {
        for (let s = 0; s < 5 && r.data['fuel_stations'].length; s++) {
          let station_id = r.data['fuel_stations'][s];
          let rows = [];
          axios.get(`/api/fuel_stations/id=${station_id}`)
            .then((r) => {
              rows.push({
                station_name: <Link to={'/stations/' + station_id}>{r.data['station_name']}</Link>,
              });
              setStationRows((prevState) => prevState.concat(rows));
            });
        }
      }

    });
  }, []);

  console.log(data);

  const imageUrl = (url) =>{
    if(url === null){
      return tempImg;
    }
    return url;
  }

  const linkArray = (data) => {
    let array = data['energy_source'];
    let info = [];
    if (array !== undefined){
      for (let i = 0; i < array.length; i++) {
        info.push(
          <li><Link to={"/energy/" + array[i]}>{array[i]}</Link></li>
        )
      }
      return info;
    }
  }

  // const TwitterContainer = (manufacturer) => {
  //   console.log(manufacturer);
  //   return (
  //     <section className="twitterContainer">
  //       <div className="twitter-embed">
  //         <TwitterTimelineEmbed
  //           sourceType="profile"
  //           screenName={manufacturer}
  //           options={{
  //             tweetLimit: "3",
  //             width: "100%",
  //             height: "50%"
  //           }}
  //           theme="dark"
  //           noHeader="true"
  //           noBorders="true"
  //           noFooter="true"
  //         ></TwitterTimelineEmbed>
  //       </div>
  //     </section>
  //   );
  // };

  const stationColumns = [
    {
      title: 'Name',
      field: 'station_name',
    },
  ];



  const tableIcons = {
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
  };

  return(
    <div>
      <Container>
        <Row>
            <Col>
              <img src={imageUrl(data['vehicle_image_url'])} className="img-fluid " alt="car image"/>
              <hr/>
              <a class="twitter-timeline" href={"https://twitter.com/" + data['manufacturer'] + "?ref_src=twsrc%5Etfw"}><img src={twitterLogo} width="30" height="30"/></a> 
              <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
            </Col>
            <Col>
              <h2>{data['name']}</h2>
              <ul>
              <li>Vehicle Type: {data['car_type']}</li>
              <li>Manufacturer: {data['manufacturer']}</li>
              <li>Fuel Type: {data['fuel_type']}</li>
              <li>Annual Fuel Cost: ${data['annual_cost']}</li>
              <li>Combined MPG (or equivalent): {data['mpg']} mpg</li>
              <li>Model Year: {data['model_year']}</li>
              <li>CO2 Emissions: {data['emissions']} grams/mile</li>
              <li>Range: {data['range_mi']} miles</li>
              <li>EPA Fuel economy Score:	{data['epa_fuel_economy_score']}</li>
              <li>Engine Size:	{data['engine_size']}</li>
              <li>Drive Type:	{data['drive_type']}</li>
              <li>How much you save compared: ${data['avg_savings']}</li>
              <li>Energy Sources: <ul>{linkArray(data)}</ul></li>
              </ul>
            </Col>
        </Row>
        <br />
        <MaterialTable title="Some Fuel Stations" data={stationRows} columns={stationColumns} icons={tableIcons} options={{ exportButton: false }} />
      </Container>
    </div>
  )
  }
  
  export default VehicleInstance;
  