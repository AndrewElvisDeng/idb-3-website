import React from 'react';
import TestRenderer from 'react-test-renderer';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
Enzyme.configure({ adapter: new Adapter() });
import { shallow } from 'enzyme';

// Import our pages
import About from '../pages/About';
import Vehicles from '../pages/Vehicles';
import Stations from '../pages/Stations';
import Energy from '../pages/Energy';
import VehicleInstance from '../pages/VehicleInstance';
import StationInstance from '../pages/StationInstance';
import EnergyInstance from '../pages/EnergyInstance';
import MemberInfo from '../components/MemberInfo'
import ApiToolInfo from '../components/ApiToolInfo'
import Pagination from '@material-ui/lab/Pagination'



it('About renders w/o crashing and has info', () => {
  const page = shallow(<About />);
  const header = <h1 className="text-center">Motivation</h1>;
  const memberInfo = <MemberInfo />;
  const apiToolInfo = <ApiToolInfo />;
  expect(page.contains(memberInfo)).toEqual(true);
  expect(page.contains(apiToolInfo)).toEqual(true);
  expect(page.contains(header)).toEqual(true);
});


it('Stations page renders w/o crashing and has info', () => {
  const page = shallow(<Stations />);
  expect(page.find("MaterialTable")).toEqual({});
});


it('Vehicles page renders w/o crashing and has info', () => {
  const page = shallow(<Vehicles />);
  expect(page.find("MaterialTable")).toEqual({});
});


it('Energy page renders w/o crashing and has info', () => {
  const testRenderer = TestRenderer.create(<Energy />);
  const testInstance = testRenderer.root;
  expect(testInstance.findByType(Pagination).props.count).toBe(3);
});


it('VehicleInstance renders w/o crashing and has info', () => {
  const page = shallow(<VehicleInstance />);
  expect(page.find("MaterialTable")).toEqual({});
});


it('StationInstance renders w/o crashing and has info', () => {
  const page = shallow(<StationInstance />);
  expect(page.find("MaterialTable")).toEqual({});
});


it('EnergyInstance renders w/o crashing and has info', () => {
  const page = shallow(<EnergyInstance />);
  expect(page.find("MaterialTable")).toEqual({});
});
