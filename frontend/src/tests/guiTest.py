from selenium import webdriver
from unittest import TestCase, main
PATH = "../../../../chromedriver_win32/chromedriver.exe"
browser = webdriver.Chrome(PATH)

class guiTest(TestCase):

    def test_home(self):
        # make sure we load the home page correctly
        browser.get('http://localhost:3000/')
        assert browser.current_url == 'http://localhost:3000/'

        # check vehicles
        path = browser.find_element_by_link_text('vehicles')
        path.click()
        assert browser.current_url == 'http://localhost:3000/vehicles'

        # check stations
        browser.get('http://localhost:3000/')
        path = browser.find_element_by_link_text('stations')
        path.click()
        assert browser.current_url == 'http://localhost:3000/stations'

        # check energy
        browser.get('http://localhost:3000/')
        path = browser.find_element_by_link_text('Concerts')
        path.click()
        assert browser.current_url == 'http://localhost:3000/energy'

        # check About
        browser.get('http://localhost:3000/')
        path = browser.find_element_by_link_text('About')
        path.click()
        assert browser.current_url == 'http://localhost:3000/about'

    def test_vehicle_model(self):
        browser.get('http://localhost:3000/vehicles')
        assert browser.current_url == 'http://localhost:3000/vehicles'

    def test_vehicle_instance(self):
        browser.get('http://localhost:3000/vehicles/12434')
        assert browser.current_url == 'http://localhost:3000/vehicles/12434'


    def test_infrastructure_model(self):
        browser.get('http://localhost:3000/stations')
        assert browser.current_url == 'http://localhost:3000/stations'

    def test_infrastructure_instance(self):
        browser.get('http://localhost:3000/stations14104')
        assert browser.current_url == 'http://localhost:3000/stations14104'

    def test_energy_model(self):
        browser.get('http://localhost:3000/energy')
        assert browser.current_url == 'http://localhost:3000/energy'

    def test_energy_intsance(self):
        browser.get('http://localhost:3000/energy/wind')
        assert browser.current_url == 'http://localhost:3000/energy/wind'

    def test_about_page(self):
        browser.get('http://localhost:3000/about')
        assert browser.current_url == 'http://localhost:3000/about'

if __name__ == "__main__":
    main()
