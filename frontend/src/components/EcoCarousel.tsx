import React, { Component } from 'react';
import Carousel from 'react-bootstrap/Carousel';
import item3 from '../images/carousel_1.png';
import item2 from '../images/carousel_2.jpg';
import item1 from '../images/carousel_3.jpg';


class EcoCarousel extends Component {
    render() {
        return (
            <Carousel>
                <Carousel.Item style={{'height':"600px"}}>
                    <img
                        className="d-block w-100"
                        src={item1}
                        alt="First slide"
                    />
                    <Carousel.Caption style={{background: "rgba(0, 0, 0, 0.4)"}}>
                        <h3>Come join us to...</h3>
                        <p>Find an ecological car!</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item style={{'height':"600px"}}>
                    <img
                        className="d-block w-100"
                        src={item2}
                        alt="Second slide"
                    />
                    <Carousel.Caption style={{background: "rgba(0, 0, 0, 0.4)"}}>
                        <h3>To...</h3>
                        <p>Learn about energy!</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item style={{'height':"600px"}}>
                    <Carousel.Caption style={{background: "rgba(0, 0, 0, 0.4)"}}>
                        <h3>And to...</h3>
                        <p>Explore your options for a greener future!</p>
                    </Carousel.Caption>
                    <img
                        className="d-block w-100"
                        src={item3}
                        alt="Third slide"
                    />
                </Carousel.Item>
            </Carousel>
        );
    }
}

export default EcoCarousel
