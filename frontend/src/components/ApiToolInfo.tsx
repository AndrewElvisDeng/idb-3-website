import React from 'react'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Grid from '@material-ui/core/Grid'

import Button from '@material-ui/core/Button';

// Gridlist from Material ui
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';


import Popover from '@material-ui/core/Popover';
import Typography from '@material-ui/core/Typography';

import ApiInfo from './ApiInfo';
import ToolInfo from './ToolInfo';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
    },
}));


function ApiToolInfo() {
    const classes = useStyles();
    
    return (
        <div>
            <br />
            <hr />
            <br />
            <Container>
            
            <div className={classes.root}>
                <Col>
                <h2 className="text-center">APIs</h2>
                <ApiInfo />
                </Col>
                <Col>
                <h2 className="text-center">Tools</h2>
                <ToolInfo />
                </Col>
            </div>
            </Container>

            <Container>
                <br />
                <hr />
                <br />
                <Row>
                    <Col>
                    
                    <Grid container justify = "center" >
        
        
                    <Button variant="contained" color="primary" href="https://gitlab.com/kcolburn38/idb-3-website">
                        GitLab Repo
                    </Button>
                    </Grid>
                    <br />
                    <Grid container justify = "center" >
                    <Button variant="contained" color="primary" href="https://documenter.getpostman.com/view/12831690/TVKHUaYQ">
                        Postman API
                    </Button>
                    </Grid>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default ApiToolInfo
