import React from 'react'

// Gridlist from Material ui
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';

// Popover from Material ui
import Popover from '@material-ui/core/Popover';
import Typography from '@material-ui/core/Typography';

const tileData = [
    {
        img: "https://miro.medium.com/max/1002/1*tYLog4b5koGAWfQV859ffQ.png",
        link: "https://developer.nrel.gov/docs/transportation/",
        title: 'NREL',
        info: "Used GET requests to grab data from api.",
        cols: 2,
    },
    {
        img: "https://lh3.googleusercontent.com/AW2Z_O3LTqlI973h7FvH3mW30sbV1Pc1BGovnnUKiFQ2j3pEQu4nKuX5Otqlk9xb_Kg",
        link: "https://www.fueleconomy.gov/feg/ws/",
        title: 'Fuel Economy',
        info: "Used GET requests to grab data from api.",
        cols: 2,
    },
    {
        img: "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1f/Energy_Information_Administration_logo.svg/1200px-Energy_Information_Administration_logo.svg.png",
        link: "https://www.eia.gov/opendata/qb.php?category=371",
        title: 'EIA',
        info: "Used GET requests to grab data from api.",
        cols: 2,
    },
];

const useStyles = makeStyles((theme) => ({
    gridList: {
        width: 500,
        height: 450,
    },
    icon: {
        color: 'rgba(255, 255, 255, 0.54)',
    },
    popover: {
        pointerEvents: 'none',
      },
    paper: {
        padding: theme.spacing(1),
    },
}));

function ApiInfo() {
    const classes = useStyles();

    const [anchorEl, setAnchorEl] = React.useState<HTMLElement | null>(null);

    const handlePopoverOpen = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
        setAnchorEl(event.currentTarget);
    };


    const handlePopoverClose = () => {
        setAnchorEl(null);
    };


    const popover = (str: string) => {
        let array = [];
        array.push(
            <Typography
                aria-owns={open ? 'mouse-over-popover' : undefined}
                aria-haspopup="true"
                onMouseEnter={handlePopoverOpen}
                onMouseLeave={handlePopoverClose}
            >
                {str}
            </Typography>
        )
        return array;
    }

    const open = Boolean(anchorEl);
    return (
        <div>
            <GridList cellHeight={180} className={classes.gridList}>
                    <GridListTile key="Subheader" cols={2} style={{ height: 'auto' }}>
                    </GridListTile>
                    {tileData.map((tile) => (
                        <GridListTile key={tile.img}>
                            <img src={tile.img} alt={tile.title} />
                            <GridListTileBar
                                title={popover(tile.title)}
                                
                                actionIcon={
                                    
                                    <a href={tile.link}>
                                    <IconButton aria-label={`info about ${tile.title}`} className={classes.icon}>
                                        
                                        <InfoIcon />
                                        
                                    </IconButton>
                                    
                                    </a>
                                }
                            />
                            <Popover
                                id="mouse-over-popover"
                                className={classes.popover}
                                classes={{
                                paper: classes.paper,
                                }}
                                open={open}
                                anchorEl={anchorEl}
                                anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'left',
                                }}
                                transformOrigin={{
                                vertical: 'top',
                                horizontal: 'left',
                                }}
                                onClose={handlePopoverClose}
                                disableRestoreFocus
                            >
                                <Typography>{tile.info}</Typography>
                            </Popover>
                        </GridListTile>
                    ))}
                </GridList>
        </div>
    )
}

export default ApiInfo;
