from unittest import main, TestCase
from tables import *

# -----------
# TestTables
# -----------

class TestTables (TestCase) :
    # ----
    # read
    # ----

    def test_vehicle_1 (self) :
        try:
            vehicle = Vehicles(
                id = "a",
            )
            self.assertFalse(True)
        except:
            self.assertTrue(True)
    
    def test_vehicle_2 (self) :
        vehicle = Vehicles(
            id = 1,
        )
        self.assertTrue(True)

    def test_vehicle_3 (self) :
        vehicle = Vehicles(
            id = 1,
            name = "test",
            car_type = "test",
            manufacturer = "test",
            fuel_type = "test",
            fuel_id = 1,
            annual_cost = 1,
            mpg = 1.0,
            model_year = 1,
            emissions = 1.0,
            range_mi = 1.0,
            epa_fuel_economy_score = 1.0,
            engine_size = "test",
            drive_type = "test",
            avg_savings = 1,
            energy_source = ["test"],
            vehicle_image_url = "test",

        )
        self.assertTrue(True)
    
    def test_vehicle_repr (self) :
        vehicle = Vehicles(
            id = 1,
            name = "test",
            car_type = "test",
            manufacturer = "test",
            fuel_type = "test",
            fuel_id = 1,
            annual_cost = 1,
            mpg = 1.0,
            model_year = 1,
            emissions = 1.0,
            range_mi = 1.0,
            epa_fuel_economy_score = 1.0,
            engine_size = "test",
            drive_type = "test",
            avg_savings = 1,
            energy_source = ["test"],
            vehicle_image_url = "test",
        )
        self.assertEqual(vehicle.__repr__(), "<Vehicle(id='1', name='test', car type='test', manufacturer='test', fuel type='test', fuel id='1', annual cost='1', mpg='1.0', model year='1', emissions='1.0', range in miles='1.0', epa fuel economy score='1.0', engine size='test', drive type='test',  avg_savings='1', energy_source='['test']', vehicle_image_url = 'test')>")
    
    def test_vehicle_dict (self) :
        vehicle = Vehicles(
            id = 1,
            name = "test",
            car_type = "test",
            manufacturer = "test",
            fuel_type = "test",
            fuel_id = 1,
            annual_cost = 1,
            mpg = 1.0,
            model_year = 1,
            emissions = 1.0,
            range_mi = 1.0,
            epa_fuel_economy_score = 1.0,
            engine_size = "test",
            drive_type = "test",
            avg_savings = 1,
            energy_source = ["test"],
            vehicle_image_url = "test",
        )
        testDict = {
            'id': 1,
            'name': "test",
            'car_type': "test",
            'manufacturer': "test",
            'fuel_type': "test",
            'fuel_id': 1,
            'annual_cost': 1,
            'mpg': 1.0,
            'model_year': 1,
            'emissions': 1.0,
            'range_mi': 1.0,
            'epa_fuel_economy_score': 1.0,
            'engine_size': "test",
            'drive_type': "test",
            'avg_savings': 1,
            'energy_source': ["test"],
            'vehicle_image_url': "test",
        }
        self.assertEqual(vehicle.get_dict(), testDict)
  
    def test_infrastructure_1 (self) :
        try:
            infrastructure = Infrastructure(
                id = "a",
            )
            self.assertFalse(True)
        except:
            self.assertTrue(True)
    
    def test_infrastructure_2 (self) :
        infrastructure = Infrastructure(
            id = 1,
        )
        self.assertTrue(True)

    def test_infrastructure_3 (self) :
        infrastructure = Infrastructure(
            id = 1, 
            station_name = "test",
            access = "test",
            public = "test",
            fuel_type = "test",
            facility_type = "test",
            address = "test",
            cards_accepted = "test",
            ev_network = "test",
            ev_charging_level = "test",
            ev_connector_type = "test",
            state = "test",
            zip_code = "test",
            country = "test",
            phone_number = "test",
            status = "test",
            energy_source = ["test"]
        )
        self.assertTrue(True)
    
    def test_infrastructure_repr (self) :
        infrastructure = Infrastructure(
            id = 1, 
            station_name = "test",
            access = "test",
            public = "test",
            fuel_type = "test",
            facility_type = "test",
            address = "test",
            cards_accepted = "test",
            ev_network = "test",
            ev_charging_level = "test",
            ev_connector_type = "test",
            state = "test",
            zip_code = "test",
            country = "test",
            phone_number = "test",
            status = "test",
            energy_source = ["test"]
        )
        self.assertEqual(infrastructure.__repr__(), "<Infrastructure(id='1', station name='test', access='test', public='test', fuel type='test', facility type='test', address='test', cards accepted='test', ev network='test', ev charging level='test', ev connector type='test', state='test', zip code='test', country='test', phone number='test', status='test', energy_source='['test']')>")
    
    def test_infrastructure_dict (self) :
        infrastructure = Infrastructure(
            id = 1, 
            station_name = "test",
            access = "test",
            public = "test",
            fuel_type = "test",
            facility_type = "test",
            address = "test",
            cards_accepted = "test",
            ev_network = "test",
            ev_charging_level = "test",
            ev_connector_type = "test",
            state = "test",
            zip_code = "test",
            country = "test",
            phone_number = "test",
            status = "test",
            energy_source = ["test"]
        )
        testDict = { 
            'id': 1,
            'station_name': "test",
            'access': "test",
            'public': "test",
            'fuel_type': "test",
            'facility_type': "test",
            'address': "test",
            'cards_accepted': "test",
            'ev_network': "test",
            'ev_charging_level': "test",
            'ev_connector_type': "test",
            'state': "test",
            'zip_code': "test",
            'country': "test",
            'phone_number': "test",
            'status': "test",
            'energy_source': ["test"],
        }
        self.assertEqual(infrastructure.get_dict(), testDict)

    def test_energy_source_1 (self) :
        try:
            energy_source = EnergySource(
                name = 1,
            )
            self.assertFalse(True)
        except:
            self.assertTrue(True)
    
    def test_energy_source_2 (self) :
        energy_source = EnergySource(
            name = "test",
        )
        self.assertTrue(True)

    def test_energy_source_3 (self) :
        energy_source = EnergySource(
            name = "test",
            supported_vehicles = [1],
            supported_charging_stations = [1],
            renewability = True,
            total_power = 1,
            electric_utility = 1,
            independent_producers = 1,
            all_commercial = 1,
            output_energy_type = "test",
            laws_incentives = "test",
            co2_emissions = 1,
            description = "test",
            image_url = "test",
            information_url = "test",
        )
        self.assertTrue(True)
    
    def test_energy_source_repr (self) :
        energy_source = EnergySource(
            name = "test",
            supported_vehicles = [1],
            supported_charging_stations = [1],
            renewability = True,
            total_power = 1,
            electric_utility = 1,
            independent_producers = 1,
            all_commercial = 1,
            output_energy_type = "test",
            laws_incentives = "test",
            co2_emissions = 1,
            description = "test",
            image_url = "test",
            information_url = "test",
        )
        self.assertEqual(energy_source.__repr__(), "<Energy Source(name='test', supported vehicles='[1]', supported charging stations='[1]', renewability='True', total power='1', electric utility='1', independent producers='1', all commercial='1', output energy type='test', laws and incentives='test', CO2 Emissions='1', description='test', image_url='test', information_url='test')>")
    
    def test_energy_source_dict (self) :
        energy_source = EnergySource(
            name = "test",
            supported_vehicles = [1],
            supported_charging_stations = [1],
            renewability = True,
            total_power = 1,
            electric_utility = 1,
            independent_producers = 1,
            all_commercial = 1,
            output_energy_type = "test",
            laws_incentives = "test",
            co2_emissions = 1,
            description = "test",
            image_url = "test",
            information_url = "test",
        )
        testDict = { 
            'name': "test",
            'supported_vehicles': [1],
            'supported_charging_stations': [1],
            'renewability': True,
            'total_power': 1,
            'electric_utility': 1,
            'independent_producers': 1,
            'all_commercial': 1,
            'output_energy_type': "test",
            'laws_incentives': "test",
            'co2_emissions': 1,
            'description': "test",
            'image_url': "test",
            'information_url': "test",
        }
        self.assertEqual(energy_source.get_dict(), testDict)

    def test_energy_source_infrastructure_1 (self) :
        try:
            energy_source_infrastructure = EnergySourceInfrastructure(
                fuel_type = 1,
            )
            self.assertFalse(True)
        except:
            self.assertTrue(True)
    
    def test_energy_source_infrastructure_2 (self) :
        energy_source_infrastructure = EnergySourceInfrastructure(
            fuel_type = "test",
        )
        self.assertTrue(True)

    def test_energy_source_infrastructure_3 (self) :
        energy_source_infrastructure = EnergySourceInfrastructure(
            fuel_type = "test",
            fuel_stations = [1]
        )
        self.assertTrue(True)
    
    def test_energy_source_infrastructure_repr (self) :
        energy_source_infrastructure = EnergySourceInfrastructure(
            fuel_type = "test",
            fuel_stations = [1]
        )
        self.assertEqual(energy_source_infrastructure.__repr__(), "<Energy Source Infrastructure(fuel_type='test', fuel_stations = '[1]')>")
    
    def test_energy_source_infrastructure_dict (self) :
        energy_source_infrastructure = EnergySourceInfrastructure(
            fuel_type = "test",
            fuel_stations = [1]
        )
        testDict = {
            'fuel_type': "test",
            'fuel_stations': [1],
        }
        self.assertEqual(energy_source_infrastructure.get_dict(), testDict)

    def test_infrastructure_vehicles_1 (self) :
        try:
            infrastructure_vehicles = InfrastructureVehicles(
                fuel_type = 1,
            )
            self.assertFalse(True)
        except:
            self.assertTrue(True)
    
    def test_infrastructure_vehicles_2 (self) :
        infrastructure_vehicles = InfrastructureVehicles(
            fuel_type = "test",
        )
        self.assertTrue(True)

    def test_infrastructure_vehicles_3 (self) :
        infrastructure_vehicles = InfrastructureVehicles(
            fuel_type = "test",
            serviceable_vehicles = [1]
        )
        self.assertTrue(True)
    
    def test_infrastructure_vehicles_repr (self) :
        infrastructure_vehicles = InfrastructureVehicles(
            fuel_type = "test",
            serviceable_vehicles = [1]
        )
        self.assertEqual(infrastructure_vehicles.__repr__(), "<Infrastructure Vehicles(fuel_type='test', serviceable_vehicles = '[1]')>")
    
    def test_infrastructure_vehicles_dict (self) :
        infrastructure_vehicles = InfrastructureVehicles(
            fuel_type = "test",
            serviceable_vehicles = [1]
        )
        testDict = {
            'fuel_type': "test",
            'serviceable_vehicles': [1],
        }
        self.assertEqual(infrastructure_vehicles.get_dict(), testDict)


# ----
# main
# ----

if __name__ == "__main__" : #pragma: no cover
    main()
