from flask import Flask, render_template, request, send_from_directory, json, jsonify
from flask_cors import CORS
from flask_restful import Resource, Api, abort, reqparse
from sqlalchemy import Column, Integer, Boolean, String, Date, create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from tables import *

app = Flask(__name__, static_folder="frontend/build/static", template_folder="frontend/build")
CORS(app)
api = Api(app)

# Root routing
# code courteous of Caitlin Lien: https://gitlab.com/caitlinlien/cs373-sustainability.git
@app.route('/', defaults={'path': ""})
@app.route('/<path:path>')
def get_index(path):
    return render_template("index.html")


# gets the data for vehicle with vehicle_id 
class vehicle_id(Resource):
	def get(self, vehicle_id):
		engine = create_engine(getURI())
		Session = sessionmaker(bind=engine)

		s = Session()
		data = s.query(Vehicles).get(vehicle_id)

		data_dict = data.get_dict()

		data_dict.update(fuel_stations = s.query(EnergySourceInfrastructure).get(data_dict['fuel_type']).get_dict()['fuel_stations'])
		s.close()
		return json.jsonify(data_dict)
	
# gets the data for fuel station with fs_id 
class fuel_station_id(Resource):
	def get(self, fs_id):
		engine = create_engine(getURI())
		Session = sessionmaker(bind=engine)

		s = Session()
		data = s.query(Infrastructure).get(fs_id)
		data_dict = data.get_dict()

		data_dict.update(serviceable_vehicles = s.query(InfrastructureVehicles).get(data_dict['fuel_type']).get_dict()['serviceable_vehicles'])
		s.close()

		return json.jsonify(data_dict)

# gets the data for the energy source with es_name 
class energy_source_name(Resource):
	def get(self, es_name):
		
		engine = create_engine(getURI())
		Session = sessionmaker(bind=engine)

		s = Session()
		data = s.query(EnergySource).get(es_name)
		s.close()

		return json.jsonify(data.get_dict())


# lists all vehicles by id and name 
#class vehicle_list(Resource):
#	def get(self):
#		# Get a list of all vehicle ID's from the database
#		return {}

# all data for all vehicles
class all_vehicles(Resource):
	def get(self):
		engine = create_engine(getURI())
		Session = sessionmaker(bind=engine)

		s = Session()
		data = s.query(Vehicles).all()
		s.close()
		dict_data = []
		for i in data:
			dict_data.append(i.get_dict())
		return json.jsonify(dict_data)

# all data for all fuel stations

class all_fuel_stations(Resource):
	def get(self):
		engine = create_engine(getURI())
		Session = sessionmaker(bind=engine)

		s = Session()
		data = s.query(Infrastructure).all()
		s.close()
		dict_data = []
		for i in data:
			dict_data.append(i.get_dict())
		return json.jsonify(dict_data)


# all data for all energy sources
class all_sources(Resource):
	def get(self):
		engine = create_engine(getURI())
		Session = sessionmaker(bind=engine)

		s = Session()
		#data = s.query(EnergySource.name, EnergySource.renewability, EnergySource.total_power, EnergySource.electric_utility, EnergySource.independent_producers, EnergySource.all_commercial, EnergySource.output_energy_type, EnergySource.laws_incentives, EnergySource.co2_emissions, EnergySource.description, EnergySource.image_url, EnergySource.information_url).all()
		data = s.query(EnergySource).all()
		s.close()
		dict_data = []
		for i in data:
			dict_data.append(i.get_dict_model_page())
		return json.jsonify(dict_data)

# model pages
api.add_resource(all_vehicles, '/api/vehicles')
api.add_resource(all_fuel_stations, '/api/fuel_stations')
api.add_resource(all_sources, '/api/energy_sources')

# instance pages
api.add_resource(vehicle_id, '/api/vehicles/id=<int:vehicle_id>', endpoint='vehicle_id')
api.add_resource(fuel_station_id, '/api/fuel_stations/id=<int:fs_id>', endpoint='fs_id')
api.add_resource(energy_source_name, '/api/energy_sources/name=<string:es_name>',endpoint='es_name')

if __name__ == "__main__": 
	app.run(host ='0.0.0.0', port = 80, debug = True) 
