#makes get requests, cleans data, and posts to the econyoom database
import requests
# import tables
import json
import xmltodict
from sqlalchemy import Column, Integer, Boolean, String, Date, create_engine

f = open('failedReads.txt', 'w')
f.write('')
f.close()

### Vehicles
renames = {
    'A8L': 'A8 L',
    'RS7': 'RS 7',
    'X5 sDrive40i 2WD': 'X5 sDrive40i'
}
# get all vehicle data from nrel
nrel_vehicle = requests.get("https://developer.nrel.gov/api/vehicles/v1/vehicles.json?api_key=ut7K5JJIJvS1JR0dm5J5dl0Sntu4B1RUDqZEfFp9")

# get the model, model year, and manufacturer for each vehicle
nrel_vehicle = nrel_vehicle.json()['result']
econyoom_vehicle_list = []
for d in nrel_vehicle:
    vehicle = {}
    vehicle['id'] = d["id"] 
    vehicle['name'] = d["model"] 
    #not all instances have these fields:
    vehicle['model_year'] = d["model_year"] if "category_name" in d else 'N/A'
    vehicle['engine_size'] = d['engine_size'] if "category_name" in d else 'N/A'
    vehicle['car_type'] = d["category_name"] if "category_name" in d else 'N/A'
    vehicle['manufacturer'] = d["manufacturer_name"] if "manufacturer_name" in d else 'N/A'
    vehicle['fuel_type'] = d["fuel_name"] if "fuel_name" in d else 'N/A'
    vehicle['fuel_id'] = d["fuel_id"] if "fuel_id" in d else 'N/A'
    econyoom_vehicle_list.append(vehicle)

# for each vehicle we have, pull the rest of the data if it's available with the make, model, and year we have available so far
for d in econyoom_vehicle_list:
    v_year = d['model_year']
    v_make = d['manufacturer']
    if d['name'] in renames:
        d['name'] = renames[d['name']]
    v_model = d['name'].replace(' ', '%20')

    #get fueleconomy's API id for the vehicle
    fueleconomy_vehicle = requests.get(f'https://www.fueleconomy.gov/ws/rest/vehicle/menu/options?year={v_year}&make={v_make}&model={v_model}')
    fueleconomy_vehicle = xmltodict.parse(fueleconomy_vehicle.content)
    print('fueleconomy', v_make, d['name'], v_year)
    print(f'https://www.fueleconomy.gov/ws/rest/vehicle/menu/options?year={v_year}&make={v_make}&model={v_model}')
    try:
        fueleconomy_vehicle = fueleconomy_vehicle['menuItems']['menuItem']['value']
    except KeyError:
        fueleconomy_vehicle = fueleconomy_vehicle['menuItems']['menuItem'][0]['value']
    except TypeError:
        try:
            fueleconomy_vehicle = fueleconomy_vehicle['menuItems']['menuItem'][0]['value']
        #model name mismatch between APIs
        except TypeError:
            file = open('failedReads.txt', 'a')
            file.write(f"fueleconomy\t{v_make}\t{d['name']}\t{v_year}\n")
            file.close()
            continue
    
    #get fueleconomy's vehicle data from API id
    print(f'https://fueleconomy.gov/ws/rest/vehicle/{fueleconomy_vehicle}')
    fueleconomy_vehicle = requests.get(f'https://fueleconomy.gov/ws/rest/vehicle/{fueleconomy_vehicle}')
    fueleconomy_vehicle = xmltodict.parse(fueleconomy_vehicle.content)
    fueleconomy_vehicle = fueleconomy_vehicle['vehicle']

    #load to EcoNyoom data. Documentation: https://www.fueleconomy.gov/feg/ws/index.shtml
    d['annual_cost'] = fueleconomy_vehicle['fuelCost08']
    d['mpg'] = fueleconomy_vehicle['cityCD']# + fueleconomy_vehicle['highwaympgCD']
    d['range_mi'] = fueleconomy_vehicle['rangeCityA']
    d['drive_type'] = fueleconomy_vehicle['drive']
    d['avg_savings'] = fueleconomy_vehicle['youSaveSpend']
    d['emissions'] = fueleconomy_vehicle['co2']

    try:
        fueleconomy_vehicle = fueleconomy_vehicle['emissionsList']['emissionsInfo']
        d['epa_fuel_economy_score'] = fueleconomy_vehicle['score']#WIP: this looks like it belongs in fuel types...?
    except TypeError:
        d['epa_fuel_economy_score'] = 'N/A'

for d in econyoom_vehicle_list:
    print(d)
    vehicle = Vehicle(
        id = d["id"],
        name = d["model"],
        car_type = d["category_name"], 
        manufacturer = d["manufacturer_name"], 
        fuel_type = d["fuel_name"], 
        fuel_id = d["fuel_id"], 
        annual_cost = d["annual_cost"], 
        mpg = d["mpg"], 
        emissions = d["emissions"], 
        range_mi = d["range_mi"], 
        epa_fuel_economy_score = d["epa_fuel_economy_score"], 
        engine_size = d["engine_size"], 
        charging = d["charging"], 
        drive_type = d["drive_type"], 
        avg_savings = d["avg_savings"], 
    )
    # s.add(vehicle)
    # s.commit()






### Fueling Stations
fuel_stations_request = requests.get("https://developer.nrel.gov/api/alt-fuel-stations/v1.json?api_key=ut7K5JJIJvS1JR0dm5J5dl0Sntu4B1RUDqZEfFp9")

nrel_fuel_stations = fuel_stations_request.json()['fuel_stations']
econyoom_fuel_station_list = []
id_gen = 1
for d in nrel_fuel_stations:
    fuel_station = {}

    fuel_station['id'] = id_gen
    id_gen += 1

    fuel_station['station_name'] = d["station_name"] if "station_name" in d else 'N/A'
    fuel_station['access'] = d["access_days_time"] if "access_days_time" in d else 'N/A'
    fuel_station['public'] = d["public"] if "public" in d else 'N/A'
    fuel_station['fuel_type'] = d["fuel_type_code"] if "fuel_type_code" in d else 'N/A'
    fuel_station['fuel_id'] = d["fuel_id"] if "fuel_id" in d else 'N/A'
    fuel_station['facility_type'] = d["facility_type"] if "facility_type" in d else 'N/A'
    fuel_station['address'] = d["street_address"] if "street_address" in d else 'N/A'
    fuel_station['cards_accepted'] = d["cards_accepted"] if "cards_accepted" in d else 'N/A'
    fuel_station['ev_network'] = d["ev_network"] if "ev_network" in d else 'N/A'
    fuel_station['ev_charging_level'] = d["ev_charging_level"] if "ev_charging_level" in d else 'N/A'
    fuel_station['ev_connector_type'] = d["ev_connector_type"] if "ev_connector_type" in d else 'N/A'
    fuel_station['state'] = d["state"] if "state" in d else 'N/A'
    fuel_station['zip_code'] = d["zip"] if "zip" in d else 'N/A'
    fuel_station['country'] = d["country"] if "country" in d else 'N/A'
    fuel_station['phone_number'] = d["station_phone"] if "station_phone" in d else 'N/A'
    fuel_station['status'] = d["status_code"] if "status_code" in d else 'N/A'
    fuel_station['source'] = d["ev_renewable_source"] if "ev_renewable_source" in d else 'N/A'
    fuel_station['serviceable_vehicle_models'] = d["fuel_type_code"] if "fuel_type_code" in d else 'N/A'

    econyoom_fuel_station_list.append(fuel_station)




### Energy Sources

econyoom_energy_source_list = []

## Coal
coal = {}
totalpower_coal_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.COW-US-98.A")
totalpower_coal_json = totalpower_coal_req.json()['series']['data']
elecutil_coal_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.COW-US-1.A")
elecutil_coal_json = elecutil_coal_req.json()['series']['data']
indprod_coal_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.COW-US-94.A")
indprod_coal_json = indprod_coal_req.json()['series']['data']
allcom_coal_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.COW-US-96.A")
allcom_coal_json = allcom_coal_req.json()['series']['data']
totalco2_coal_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=EMISS.CO2-TOTV-TT-CO-US.A")
totalco2_coal_json = totalco2_coal_req.json()['series']['data']


pet_laws_req = requests.get("https://developer.nrel.gov/api/transportation-incentives-laws/v1.json?api_key=ut7K5JJIJvS1JR0dm5J5dl0Sntu4B1RUDqZEfFp9&technology=BIOD&recent=false&expired=false&local=false")

coal['name'] = "Coal"
coal['supported_vehicles'] = Column(String)
coal['supported_charging_stations'] = Column(String)
coal['renewability'] = True

coal['total_power'] = Column(Integer)
coal['electric_utility'] = Column(Integer)
coal['independent_producers'] = Column(Integer)
coal['all_commercial'] = Column(Integer)

coal['co2_emissions'] = Column(Integer)

coal['output_energy_type'] = "Heat into electricity"
coal['laws_incentives'] = Column(String)
coal['description'] = Column(String)

econyoom_energy_source_list.append(coal)

## Natural Gas
cng = {}
totalpower_cng_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.NG-US-98.A")
totalpower_cng_json = totalpower_cng_req.json()['series']['data']
elecutil_cng_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.NG-US-1.A")
elecutil_cng_json = elecutil_cng_req.json()['series']['data']
indprod_cng_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.NG-US-94.A")
indprod_cng_json = indprod_cng_req.json()['series']['data']
allcom_cng_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.NG-US-96.A")
allcom_cng_json = allcom_cng_req.json()['series']['data']
totalco2_cng_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=EMISS.CO2-TOTV-TT-NG-US.A")
totalco2_cng_json = totalco2_cng_req.json()['series']['data']

pet_laws_req = requests.get("https://developer.nrel.gov/api/transportation-incentives-laws/v1.json?api_key=ut7K5JJIJvS1JR0dm5J5dl0Sntu4B1RUDqZEfFp9&technology=NG&recent=false&expired=false&local=false")

cng['name'] = "Natrual Gas"
cng['supported_vehicles'] = Column(String)
cng['supported_charging_stations'] = Column(String)
cng['renewability'] = False

cng['total_power'] = Column(Integer)
cng['electric_utility'] = Column(Integer)
cng['independent_producers'] = Column(Integer)
cng['all_commercial'] = Column(Integer)

cng['co2_emissions'] = Column(Integer)

cng['output_energy_type'] = "Directly as fuel, or heat into electricity"
cng['laws_incentives'] = Column(String)
cng['description'] = Column(String)

econyoom_energy_source_list.append(cng)

## Petroleum Liquids
pet = {}
totalpower_pet_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.PEL-US-98.A")
totalpower_pet_json = totalpower_cng_req.json()['series']['data']
elecutil_pet_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.PEL-US-1.A")
elecutil_pet_json = elecutil_pet_req.json()['series']['data']
indprod_pet_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.PEL-US-94.A")
indprod_pet_json = indprod_pet_req.json()['series']['data']
allcom_pet_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.PEL-US-96.A")
allcom_pet_json = allcom_pet_req.json()['series']['data']
totalco2_pet_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=EMISS.CO2-TOTV-TT-PE-US.A")
totalco2_pet_json = totalco2_pet_req.json()['series']['data']

pet_laws_req = requests.get("https://developer.nrel.gov/api/transportation-incentives-laws/v1.json?api_key=ut7K5JJIJvS1JR0dm5J5dl0Sntu4B1RUDqZEfFp9&technology=BIOD&recent=false&expired=false&local=false")

pet['name'] = "Liquid Petroleum"
pet['supported_vehicles'] = Column(String)
pet['supported_charging_stations'] = Column(String)
pet['renewability'] = False

pet['total_power'] = Column(Integer)
pet['electric_utility'] = Column(Integer)
pet['independent_producers'] = Column(Integer)
pet['all_commercial'] = Column(Integer)

pet['co2_emissions'] = Column(Integer)

pet['output_energy_type'] = "Processed into fuels or used to generate electricity."
pet['laws_incentives'] = Column(String)
pet['description'] = Column(String)

econyoom_energy_source_list.append(pet)

## Petroleum Coke
pec = {}
totalpower_pec_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.PC-US-99.A")
totalpower_pec_json = totalpower_pec_req.json()['series']['data']
elecutil_pec_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.PC-US-1.A")
elecutil_pec_json = elecutil_pec_req.json()['series']['data']
indprod_pec_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.PC-US-94.A")
indprod_pec_json = indprod_pec_req.json()['series']['data']
allcom_pec_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.PC-US-96.A")
allcom_pec_json = allcom_pec_req.json()['series']['data']
totalco2_pec_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=EMISS.CO2-TOTV-TT-PE-US.A")
totalco2_pec_json = totalco2_pec_req.json()['series']['data']


pec['name'] = "Petroleum Coke"
pec['supported_vehicles'] = Column(String)
pec['supported_charging_stations'] = Column(String)
pec['renewability'] = False

pec['total_power'] = Column(Integer)
pec['electric_utility'] = Column(Integer)
pec['independent_producers'] = Column(Integer)
pec['all_commercial'] = Column(Integer)

pec['co2_emissions'] = Column(Integer)

pec['output_energy_type'] = "Heat into electricity"
pec['laws_incentives'] = Column(String)
pec['description'] = Column(String)

econyoom_energy_source_list.append(pec)

## Nuclear
nuc = {}
totalpower_nuc_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.NUC-US-98.A")
totalpower_nuc_json = totalpower_pec_req.json()['series']['data']
elecutil_nuc_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.NUC-US-1.A")
elecutil_nuc_json = elecutil_nuc_req.json()['series']['data']
indprod_pet_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.NUC-US-94.A")
indprod_nuc_json = indprod_pet_req.json()['series']['data']
# Data unavailable: allcom_pet_req 

pet_laws_req = requests.get("https://developer.nrel.gov/api/transportation-incentives-laws/v1.json?api_key=ut7K5JJIJvS1JR0dm5J5dl0Sntu4B1RUDqZEfFp9&technology=ELEC&recent=false&expired=false&local=false")

nuc['name'] = "Nuclear"
nuc['supported_vehicles'] = Column(String)
nuc['supported_charging_stations'] = Column(String)
nuc['renewability'] = True

nuc['total_power'] = Column(Integer)
nuc['electric_utility'] = Column(Integer)
nuc['independent_producers'] = Column(Integer)
nuc['all_commercial'] = "Data Unavailable"

nuc['co2_emissions'] = "N/A (Nuclear Waste generated instead)"

nuc['output_energy_type'] = "Fusion reaction generates heat into electricity"
nuc['laws_incentives'] = Column(String)
nuc['description'] = Column(String)

econyoom_energy_source_list.append(nuc)

## Hydroelectric
hyc = {}
totalpower_hyc_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.HYC-US-98.A")
totalpower_hyc_json = totalpower_pec_req.json()['series']['data']
elecutil_hyc_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.HYC-US-1.A")
elecutil_hyc_json = elecutil_hyc_req.json()['series']['data']
indprod_hyc_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.HYC-US-94.A")
indprod_hyc_json = indprod_hyc_req.json()['series']['data']
allcom_hyc_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.HYC-US-96.A")
allcom_hyc_json = allcom_hyc_req.json()['series']['data']

pet_laws_req = requests.get("https://developer.nrel.gov/api/transportation-incentives-laws/v1.json?api_key=ut7K5JJIJvS1JR0dm5J5dl0Sntu4B1RUDqZEfFp9&technology=ELEC&recent=false&expired=false&local=false")

hyc['name'] = "Hydroelectric"
hyc['supported_vehicles'] = Column(String)
hyc['supported_charging_stations'] = Column(String)
hyc['renewability'] = True

hyc['total_power'] = Column(Integer)
hyc['electric_utility'] = Column(Integer)
hyc['independent_producers'] = Column(Integer)
hyc['all_commercial'] = Column(Integer)

hyc['co2_emissions'] = "N/A"

hyc['output_energy_type'] = "Movement of water into electricity"
hyc['laws_incentives'] = Column(String)
hyc['description'] = Column(String)

econyoom_energy_source_list.append(hyc)

## Wind
wnd = {}
totalpower_wnd_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.WND-US-98.A")
totalpower_wnd_json = totalpower_wnd_req.json()['series']['data']
elecutil_wnd_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.WND-US-1.A")
elecutil_wnd_json = elecutil_wnd_req.json()['series']['data']
indprod_wnd_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.WND-US-94.A")
indprod_wnd_json = indprod_wnd_req.json()['series']['data']
allcom_wnd_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.WND-US-96.A")
allcom_wnd_json = allcom_wnd_req.json()['series']['data']

pet_laws_req = requests.get("https://developer.nrel.gov/api/transportation-incentives-laws/v1.json?api_key=ut7K5JJIJvS1JR0dm5J5dl0Sntu4B1RUDqZEfFp9&technology=ELEC&recent=false&expired=false&local=false")

wnd['name'] = "Wind"
wnd['supported_vehicles'] = Column(String)
wnd['supported_charging_stations'] = Column(String)
wnd['renewability'] = True

wnd['total_power'] = Column(Integer)
wnd['electric_utility'] = Column(Integer)
wnd['independent_producers'] = Column(Integer)
wnd['all_commercial'] = Column(Integer)

wnd['co2_emissions'] = "N/A"

wnd['output_energy_type'] = "Movement of air into electricity"
wnd['laws_incentives'] = Column(String)
wnd['description'] = Column(String)

econyoom_energy_source_list.append(wnd)

## Geothermal
geo = {}
totalpower_geo_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.GEO-US-98.A")
totalpower_geo_json = totalpower_geo_req.json()['series']['data']
elecutil_geo_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.GEO-US-1.A")
elecutil_geo_json = elecutil_geo_req.json()['series']['data']
indprod_geo_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.GEO-US-94.A")
indprod_geo_json = indprod_geo_req.json()['series']['data']
allcom_geo_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.GEO-US-96.A")
allcom_geo_json = allcom_geo_req.json()['series']['data']

pet_laws_req = requests.get("https://developer.nrel.gov/api/transportation-incentives-laws/v1.json?api_key=ut7K5JJIJvS1JR0dm5J5dl0Sntu4B1RUDqZEfFp9&technology=ELEC&recent=false&expired=false&local=false")

geo['name'] = "Geothermal"
geo['supported_vehicles'] = Column(String)
geo['supported_charging_stations'] = Column(String)
geo['renewability'] = True

geo['total_power'] = Column(Integer)
geo['electric_utility'] = Column(Integer)
geo['independent_producers'] = Column(Integer)
geo['all_commercial'] = Column(Integer)

geo['co2_emissions'] = "N/A"

geo['output_energy_type'] = "Heat from the Earth into electricity"
geo['laws_incentives'] = Column(String)
geo['description'] = Column(String)

econyoom_energy_source_list.append(geo)

## Wood and wood-derived Fuels
wod = {}
totalpower_wod_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.WWW-US-98.A")
totalpower_wod_json = totalpower_wod_req.json()['series']['data']
elecutil_wod_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.WWW-US-1.A")
elecutil_wod_json = elecutil_wod_req.json()['series']['data']
indprod_wod_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.WWW-US-94.A")
indprod_wod_json = indprod_wod_req.json()['series']['data']
allcom_wod_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.WWW-US-96.A")
allcom_wod_json = allcom_wod_req.json()['series']['data']

pet_laws_req = requests.get("https://developer.nrel.gov/api/transportation-incentives-laws/v1.json?api_key=ut7K5JJIJvS1JR0dm5J5dl0Sntu4B1RUDqZEfFp9&technology=ELEC&recent=false&expired=false&local=false")

wod['name'] = "Wood and wood derived fuels"
wod['supported_vehicles'] = Column(String)
wod['supported_charging_stations'] = Column(String)
wod['renewability'] = True

wod['total_power'] = Column(Integer)
wod['electric_utility'] = Column(Integer)
wod['independent_producers'] = Column(Integer)
wod['all_commercial'] = Column(Integer)

wod['co2_emissions'] = Column(Integer)

wod['output_energy_type'] = "Heat from burning into electricity"
wod['laws_incentives'] = Column(String)
wod['description'] = Column(String)

econyoom_energy_source_list.append(wod)

## Other Biomass
wst = {}
totalpower_was_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.WAS-US-98.A")
totalpower_was_json = totalpower_was_req.json()['series']['data']
elecutil_was_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.WAS-US-1.A")
elecutil_was_json = elecutil_was_req.json()['series']['data']
indprod_was_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.WAS-US-94.A")
indprod_was_json = indprod_was_req.json()['series']['data']
allcom_was_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.WAS-US-96.A")
allcom_was_json = allcom_was_req.json()['series']['data']

pet_laws_req = requests.get("https://developer.nrel.gov/api/transportation-incentives-laws/v1.json?api_key=ut7K5JJIJvS1JR0dm5J5dl0Sntu4B1RUDqZEfFp9&technology=BIOD&recent=false&expired=false&local=false")


wst['name'] = "Biomass"
wst['supported_vehicles'] = Column(String)
wst['supported_charging_stations'] = Column(String)
wst['renewability'] = True

wst['total_power'] = Column(Integer)
wst['electric_utility'] = Column(Integer)
wst['independent_producers'] = Column(Integer)
wst['all_commercial'] = Column(Integer)

wst['co2_emissions'] = Column(Integer)

wst['output_energy_type'] = "Heat from burning into electricity"
wst['laws_incentives'] = Column(String)
wst['description'] = Column(String)

econyoom_energy_source_list.append(wst)

## Solar
sun = {}
totalpower_sun_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.SUN-US-98.A")
totalpower_sun_json = totalpower_sun_req.json()['series']['data']
elecutil_sun_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.SUN-US-1.A")
elecutil_sun_json = elecutil_sun_req.json()['series']['data']
indprod_sun_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.SUN-US-94.A")
indprod_sun_json = indprod_sun_req.json()['series']['data']
allcom_sun_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.SUN-US-96.A")
allcom_sun_json = allcom_sun_req.json()['series']['data']

pet_laws_req = requests.get("https://developer.nrel.gov/api/transportation-incentives-laws/v1.json?api_key=ut7K5JJIJvS1JR0dm5J5dl0Sntu4B1RUDqZEfFp9&technology=ELEC&recent=false&expired=false&local=false")

sun['name'] = "Solar"
sun['supported_vehicles'] = Column(String)
sun['supported_charging_stations'] = Column(String)
sun['renewability'] = True

sun['total_power'] = Column(Integer)
sun['electric_utility'] = Column(Integer)
sun['independent_producers'] = Column(Integer)
sun['all_commercial'] = Column(Integer)

sun['co2_emissions'] = "N/A"

sun['output_energy_type'] = "Solar energy into electricity"
sun['laws_incentives'] = Column(String)
sun['description'] = Column(String)

econyoom_energy_source_list.append(sun)

#get num closed issues for each dev
for dev in devs:
    uname = devs[dev]['uname']
    request = requests.get(f'https://gitlab.com/api/v4/projects/21238733/issues_statistics?assignee_username={uname}')
    devs[dev]['closedIssues'] = request.json()['statistics']['counts']['closed']
#.json() returns json as a dict

#get the commits from each user
commitResponse = requests.get('https://gitlab.com/api/v4/projects/21238733/repository/commits').json()
totalCommits = len(commitResponse)





# json_response = response.json()
# repository = json_response['items'][0]
# print(f'Repository name: {repository["name"]}')  # Python 3.6+
# print(f'Repository description: {repository["description"]}')  # Python 3.6+


