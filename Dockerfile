FROM nikolaik/python-nodejs

RUN git clone https://gitlab.com/kcolburn38/idb-3-website.git

WORKDIR /idb-3-website

RUN git pull --force
RUN cd frontend && yarn install && yarn build
RUN pip3 install -r requirements.txt

EXPOSE 80

CMD make docker-image-run

