.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules
SHELL         := bash


##### DOCKER #####

# Runs the backend docker image
docker-run:
	docker run \
		--name econyoom_container \
		--rm \
		-d \
		-p 80:5000 \
		econyoom

docker-image-run:
	git pull --force
	docker run \
		--name econyoom_container \
		--rm \
		-d \
		-p 80:5000 \
		econyoom

# Stops the backend docker container
docker-stop:
	docker stop econyoom_container

# Builds the backend docker image 
docker-build:
	docker build -t backend:latest .

# Builds/rebuilds the docker-compose container
docker-compose-build:
	docker-compose build

# Create and start container within docker-compose container
docker-compose-up:
	docker-compose up

# Removes the docker-compose container
docker-compose-down:
	docker-compose down



##### LOCAL #####

# runs the frontend app locally
run-frontend:
	cd frontend && yarn start

# sets up installations and dependencies required for frontend
frontend:
	cd frontend && yarn install 
	cd frontend && yarn build

# tests for frontend
frontend-tests:

# runs the backend app locally
run-backend:
	# export FLASK_APP=api.py
	# flask run
	python3 api.py

# sets up installations and dependencies required for backend
backend:
	cd frontend && yarn install && yarn build
	pip3 install -r requirements.txt

backend-tests: 
	python3 tables_test.py

all:
